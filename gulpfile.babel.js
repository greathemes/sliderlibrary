/**
 * Gulp file.
 */

import gulp from 'gulp';
import webpack from 'webpack-stream';
import del from 'del';
import browsersync from 'browser-sync';

const server = browsersync.create();

// Server.
export const serve = ( done ) => {

	server.init( { proxy: 'http://localhost:8888/sliderLibrary' } );
	done();

}

// Reload.
export const reload = ( done ) => {

	server.reload();
	done();

}

// Clean assets js.
export const clean = () => del( [ 'assets/js' ], { force: true } );

export const scripts = ( done ) => {

	return gulp.src( 'js/slider.js' )
		.pipe( webpack( require( './webpack.config.js' ) ) )
		.pipe( gulp.dest( 'assets/js' ) );

}

// Watch.
export const watch = () => {

	gulp.watch( 'js/**/*.js', gulp.series( scripts, reload ) );

}

// Tasks.
export const dev = gulp.series( clean, scripts, serve, watch );
export default dev;
