import { Slider as S } from '../slider-class';

/**
 * Add active class to the slide/slides.
 */
export function activateSlides() {

	const
	children       = S.getSlides(),
	childrenLength = children.length,
	activeIndexes  = S.getSlideIndexes();

	for ( let i = 0; i < childrenLength; i++ ) {
		if ( activeIndexes.includes( i ) ) {
			children[ i ].classList.add( 'active' );
		} else {
			children[ i ].classList.remove( 'active' );
		}
	}

}

/**
 * 
 */
export function reindex() {

	S.setInitialSlideIndexes();
	S.incrementSlideIndexes( S.getFirstSlideIndex() );

	while ( S.getSlides( S.getSlideIndexes( S.getSlideIndexes().length - 1 ) ) === undefined ) {
		S.decrementIndexes();
	}

}

export function removeCloned() {

	const clonedSlides = S.getClonedSlides();

	if ( clonedSlides.length > 0 ) {

		for ( const slide of clonedSlides ) {
			slide.remove();
		}

		clonedSlides.length = 0;

		S.setSlideIndexes( S.getClonedSlideIndexes() );

	}

}

/**
 * 
 */
export function clone() {

	const
	slidesAtOnce = S.getSlidesAtOnce(),
	clonedSlides = [];

	// Removes clones and later creates new ones if needed.
	removeCloned();

	// Clear indexes.
	S.setInitialSlideIndexes();
	S.setMaxIndex();

	let
	slidesNumber      = S.getSlidesNumber(),
	initialSlideIndex = 0;

	if ( slidesNumber < slidesAtOnce ) {

		const
		slides       = S.getSlides(),
		slidesLenght = slides.length;

		while ( slidesNumber < slidesAtOnce ) {

			const cloned = slides[ initialSlideIndex ].cloneNode( true );
			cloned.classList.add( 'cloned' );

			S.getSelector().appendChild( cloned );
			clonedSlides.push( cloned );

			initialSlideIndex++;
			slidesNumber++;

			if ( initialSlideIndex === slidesLenght ) {
				initialSlideIndex = 0;
			}

		}

		S.setClonedSlides( clonedSlides );
		S.setClonedSlideIndexes( S.getSlideIndexes() );

		reindex();

	}

}
