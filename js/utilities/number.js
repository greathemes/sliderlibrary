/**
 * Checks whether the value is a positive number or equal to zero.
 *
 * @param {Number} value
 */
export function isPositiveOrZero( value ) {

	return Number.isInteger( value ) || value === 0;

}
