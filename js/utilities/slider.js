import { Slider as S } from '../slider-class';

/**
 * Sets the width of the slider depending on the number of slides.
 */
export function sliderWidth() {

	const sliderWidth = S.getSlidesNumber() * 100 / S.getSlidesAtOnce();

	S.getSelector().style.width = `${sliderWidth}%`;

}

/**
 * Update the slider position.
 */
export function sliderPosition() {

	const sliderPosition = S.getFirstSlideIndex() * ( 100 / S.getSlidesNumber() );

	S.getSelector().style.transform = `translateX(-${sliderPosition}%)`;

}

/**
 * Sets the slider speed.
 */
export function sliderSpeed() {

	const speed = S.getSpeed() / 1000;

	// Add transition-duration to slider and its container.
	S.getContainer().style.transitionDuration = `${speed}s`;
	S.getSelector().style.transitionDuration = `${speed}s`;

	// Add transition-duration to each slide.
	for ( let slide of S.getSlides() ) {
		slide.style.transitionDuration = `${speed}s`;
	}

}

export function sliderHide() {
	S.getContainer().style.opacity = '0';
}

export function sliderShow() {
	S.getContainer().style.opacity = '1';
}

