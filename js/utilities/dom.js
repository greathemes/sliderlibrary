import { Slider as S } from '../slider-class';

/**
 * Creates container for slider.
 */
export function dynamicallyCreateContainer() {

	// Create container.
	const
	container = document.createElement( 'div' ),
	selector  = S.getSelector();

	// Add classes to container.
	container.classList.add( S.getClasses().container );

	if ( S.isAutoplay() ) {
		container.classList.add( S.getClasses().container + '--autoplay' );
	}

	// Append container to the DOM.
	selector.parentNode.insertBefore( container, selector );
	container.appendChild( selector );

	S.setContainer( container );

}

/**
 * Creates container for dots and arrows slider.
 */
export function dynamicallyCreateInterface() {

	if ( S.areArrows() || S.areDots() ) {

		// Create interface.
		const
		containerInterface = document.createElement( 'div' ),
		container          = S.getContainer();

		// Add class to interface.
		containerInterface.classList.add( 'slider__interface' );

		// Append interface to the DOM.
		container.appendChild( containerInterface );

		S.setInterface( containerInterface );

	}

}
