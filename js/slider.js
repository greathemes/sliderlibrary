import { Slider } from './slider-class';

let slider = new Slider( '.slider__list', {
	breakpoints: [
		{
			mediaQuery: 800,
			slidesAtOnce: 2
		},
		{
			mediaQuery: 1200,
			slidesAtOnce: 3
		},
	]
} ).init();
