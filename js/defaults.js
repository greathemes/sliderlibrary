export default {

	/**
	 * Show/hide slider dots.
	 *
	 * @type {Boolean}
	 */
	dots: true,

	/**
	 * Class name for dots container.
	 *
	 * @type {String}
	 */
	dotsContainerClass: 'slider__dotsContainer',

	/**
	 * Class name for dots.
	 *
	 * @type {String}
	 */
	dotsClass: 'slider__dot',

	/**
	 * Class name for active dot.
	 *
	 * @type {String}
	 */
	dotsActiveClass: 'slider__dot--active',

	/**
	 * Show/hide slider arrows.
	 *
	 * @type {Boolean}
	 */
	arrows: true,

	/**
	 * Class name for arrows container.
	 *
	 * @type {String}
	 */
	arrowsContainerClass: 'slider__arrowsContainer',

	/**
	 * Class name for previous arrow.
	 *
	 * @type {String}
	 */
	arrowsPrevClass: 'slider__arrowPrev',

	/**
	 * Class name for next arrow.
	 *
	 * @type {String}
	 */
	arrowsNextClass: 'slider__arrowNext',

	/**
	 * Media query breakpoints.
	 *
	 * Example:
	 *	[
	 *		{
	 *			mediaQuery: 600,
	 *			slidesAtOnce: 2
	 *		},
	 *		{
	 *			mediaQuery: 900,
	 *			slidesAtOnce: 3
	 *		},
	 *	]
	 *
	 * @type {Arry}
	 */
	breakpoints: [],

	/**
	 * Enable/disable mobile first approach for media query breakpoints.
	 *
	 * @type {Boolean}
	 */
	mobileFirst: true,

	/**
	 * Number of slides displayed simultaneously.
	 *
	 * @type {Number}
	 */
	slidesAtOnce: 1,

	/**
	 * Class name for main container.
	 *
	 * @type {String}
	 */
	containerClass: 'slider__container',

	/**
	 * Class name for active slides.
	 *
	 * @type {String}
	 */
	activeSlidesClass: 'active',

	/**
	 * Enable/disable rewind mode.
	 *
	 * When the last slide is active and the user clicks "next", it will return to the first. The other way around it works the same.
	 *
	 * @type {Boolean}
	 */
	rewind: true,

	/**
	 * Adjusts the height dynamically.
	 *
	 * @type {Boolean}
	 */
	autoHeight: true,

	/**
	 * Enable/disable autoplay mode.
	 *
	 * @type {Boolean}
	 */
	autoplay: true,

	/**
	 * Time between next slide when autoplay.
	 *
	 * @type {Boolean}
	 */
	autoplayTime: 5000,

	/**
	 * Speed for whole slider ( animations etc. ), it's just transition-duration.
	 *
	 * @type {Boolean}
	 */
	speed: 500,

	/**
	 * Enable/disable swipe mode. To do...
	 *
	 * @type {Boolean}
	 */
	swipeable: true, // To do...

	/**
	 * Animations. To do...
	 *
	 * @type {Boolean}
	 */
	animation: true // To do...

}
