import defaults from './defaults';
import { activateSlides, reindex, clone } from './utilities/slides';
import { sliderWidth, sliderPosition, sliderSpeed, sliderHide, sliderShow } from './utilities/slider';
import { dynamicallyCreateContainer, dynamicallyCreateInterface } from './utilities/dom';
import { createArrows, rebuildArrows } from './options/arrows';
import { createDots, rebuildDots } from './options/dots';
import { autoHeightEvent, initialHeights, autoHeight } from './options/auto-height';
import { breakpoints } from './options/breakpoints';
import { setAutoplayInterval } from './options/autoplay';
import { swipe } from './options/swipeable';
import * as checkers from './helpers/checkers';
import * as getters from './helpers/getters';
import * as setters from './helpers/setters';

export class Slider {

	/**
	 * Constructor.
	 *
	 * @param {String/Object} selector - if Object, it has to be instanceof Element.
	 * @param {Object} options
	 */
	constructor( selector, options = {} ) {

		this.selector = this.selectorValidation( selector );
		this.settings = Object.assign( {}, defaults, options );

		// Add checker functions.
		for ( let checker of Object.values( checkers ) ) {
			checker.call( this );
		}

		// Add getter functions.
		for ( let getter of Object.values( getters ) ) {
			getter.call( this );
		}

		// Add setter functions.
		for ( let setter of Object.values( setters ) ) {
			setter.call( this );
		}

	}

	/**
	 * Validates the selector.
	 *
	 * @param {String/Object} selector - if Object, it has to be instanceof Element.
	 */
	selectorValidation( selector ) {

		if ( selector && selector instanceof Element ) {
			return selector;
		} else if ( typeof selector === 'string' ) {
			if ( selector.startsWith( '.' ) || selector.startsWith( '#' ) ) {
				return document.querySelector( selector );
			} else {
				return document.querySelector( '.' + selector );
			}
		}

		return selector;

	}

	rebuild() {

		sliderHide();

		const functions = [
			clone,
			rebuildDots,
			rebuildArrows,
			reindex,
			activateSlides,
			sliderPosition,
			sliderWidth
		];

		setTimeout( function() {

			for ( let fn of functions ) {
				fn();
			}

			setTimeout( function() {
				initialHeights();
				autoHeight();
				sliderShow();
			}, Slider.getSpeed() );

		}, Slider.getSpeed() );

	}

	init() {

		if ( this.selector ) {

			const functions = [
				dynamicallyCreateContainer,
				dynamicallyCreateInterface,
				sliderSpeed,
				breakpoints,
				clone,
				createDots,
				createArrows,
				reindex,
				activateSlides,
				sliderPosition,
				sliderWidth,
				autoHeightEvent,
				setAutoplayInterval,
				swipe
			];

			for ( let fn of functions ) {
				fn();
			}

		}

	}

}
