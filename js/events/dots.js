import { Slider as S } from '../slider-class';
import { sliderPosition } from '../utilities/slider';
import { activateSlides } from '../utilities/slides';
import { deactivateDot, activateDot } from '../options/dots';
import { autoHeight } from '../options/auto-height';
import { clearAutoplayInterval } from '../options/autoplay';

/**
 * Changes slide after click on button.
 */
export function changeSlideByButton() {

	const dotIndex = parseInt( this.dataset.index );

	deactivateDot();
	S.setDotIndex( dotIndex );

	// Update slides indexes.
	S.setInitialSlideIndexes();
	S.incrementSlideIndexes( dotIndex );

	activateDot();
	activateSlides();
	sliderPosition();
	autoHeight();
	clearAutoplayInterval();

}
