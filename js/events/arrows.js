import { Slider as S } from '../slider-class';
import { sliderPosition } from '../utilities/slider';
import { activateSlides } from '../utilities/slides';
import { deactivateDot, activateDot } from '../options/dots';
import { autoHeight } from '../options/auto-height';
import { rewind } from '../options/rewind';
import { clearAutoplayInterval } from '../options/autoplay';

/**
 * Changes slide after click on arrow.
 */
export function changeSlideByArrow() {

	const
	firstIndex = S.getFirstSlideIndex(),
	maxIndex   = S.getMaxSlideIndex(),
	isRewind   = S.isRewind(),
	direction  = this.dataset.direction;
	
	deactivateDot();

	if ( direction === 'prev' ) {
		if ( firstIndex === 0 && isRewind ) {
			rewind( direction );
		} else if ( firstIndex > 0 ) {
			S.decrementDot();
			S.decrementIndexes();
		}
	} else {
		if ( firstIndex === maxIndex && isRewind ) {
			rewind( direction );
		} else if ( firstIndex < maxIndex ) {
			S.incrementDot();
			S.incrementSlideIndexes();
		}
	}

	activateDot();
	activateSlides();
	sliderPosition();
	autoHeight();
	clearAutoplayInterval();

}
