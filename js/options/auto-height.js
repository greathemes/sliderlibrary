import { Slider as S } from '../slider-class';

/**
 * Set the slider height.
 */
export function autoHeightEvent() {

	if ( S.isAutoHeight() ) {

		initialHeights();
		autoHeight();

		window.addEventListener( 'resize', function() {
			initialHeights();
			autoHeight();
		} );

	}

}

/**
 * Set the initial slide heights.
 */
export function initialHeights() {

	const slides = S.getSlides();

	for ( let slide of slides ) {
		slide.style.height = '';
		slide.setAttribute( 'data-initial-height', slide.offsetHeight - parseInt( getComputedStyle( slide ).paddingTop ) - parseInt( getComputedStyle( slide ).paddingBottom ) );
	}

}

/**
 * Set the slider height.
 */
export function autoHeight() {

	if ( S.isAutoHeight() ) {

		const
		slides                   = S.getSlides(),
		activeSlides             = [ ...slides ].filter( el => el.classList.contains( S.getClasses().activeSlides ) ),
		slideHeightsNoPaddings   = activeSlides.map( el => el.offsetHeight - parseInt( getComputedStyle( el ).paddingTop ) - parseInt( getComputedStyle( el ).paddingBottom ) ),
		initialHeights           = activeSlides.map( el => el.dataset.initialHeight ),
		initialHeightsPaddings   = activeSlides.map( el => parseInt( el.dataset.initialHeight ) + parseInt( getComputedStyle( el ).paddingTop ) + parseInt( getComputedStyle( el ).paddingBottom ) ),
		maxHeightNoPaddings      = Math.max( ...slideHeightsNoPaddings ),
		maxInitialHeight         = Math.max( ...initialHeights ),
		maxInitialHeightPaddings = Math.max( ...initialHeightsPaddings );

		for ( let slide of activeSlides ) {
			if ( maxInitialHeight < maxHeightNoPaddings ) {
				slide.style.height = `${maxInitialHeight}px`;
			} else {
				slide.style.height = `${maxHeightNoPaddings}px`;
			}
		}

		S.getSelector().style.height = `${maxInitialHeightPaddings}px`;

	}

}
