import { Slider as S } from '../slider-class';
import { reindex, removeCloned } from '../utilities/slides';
import { autoHeight, initialHeights } from './auto-height';

/**
 * Slider breakpoints.
 */
export function breakpoints() {

	const breakpoints = S.getBreakpoints();

	if ( Object.getOwnPropertyNames( breakpoints ).length > 0 ) {

		const
		mobileFirst  = S.isMobileFirst(),
		mediaQueries = breakpoints.map( bp => bp.mediaQuery ),
		slidesAtOnce = breakpoints.map( bp => bp.slidesAtOnce );

		let mediaQueriesLength = mediaQueries.length;

		if ( mobileFirst ) {
			mediaQueries.unshift( 0 );
			slidesAtOnce.unshift( S.getSlidesAtOnce() );
		} else {
			mediaQueries.push( mediaQueries[ mediaQueriesLength - 1 ] + 1 );
			slidesAtOnce.push( S.getSlidesAtOnce() );
		}

		mediaQueriesLength = mediaQueries.length;

		if ( breakpoints && matchMedia ) {

			for ( let i = 0; i < mediaQueriesLength; i++ ) {

				let mq;

				if ( mobileFirst ) {
					if ( i === mediaQueriesLength - 1 ) {
						mq = window.matchMedia( `(min-width:${mediaQueries[ i ]}px)` );
					} else {
						mq = window.matchMedia( `(min-width:${mediaQueries[ i ]}px) and (max-width:${mediaQueries[ i + 1 ] - 1}px)` );
					}
				} else {
					if ( i === 0 ) {
						mq = window.matchMedia( `(max-width:${mediaQueries[ i ]}px)` );
					} else if ( i === mediaQueriesLength - 1 ) {
						mq = window.matchMedia( `(min-width:${mediaQueries[ mediaQueriesLength - 1 ]}px)` );
					} else {
						mq = window.matchMedia( `(max-width:${mediaQueries[ i ]}px) and (min-width:${mediaQueries[ i - 1 ] + 1}px)` );
					}
				}

				if ( mq.matches ) {
					S.setSlidesAtOnce( slidesAtOnce[ i ] );
					reindex();
				}

				mq.addListener( function( mq ) {

					if ( mq.matches ) {
						S.setSlidesAtOnce( slidesAtOnce[ i ] );
						S.prototype.rebuild();
					}

				} );

			}

		}

		S.setMaxIndex();

	}

}
