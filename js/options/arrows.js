import { Slider as S } from '../slider-class';
import { changeSlideByArrow } from '../events/arrows';

/**
 * Creates arrows for slider.
 */
export function createArrows() {

	if ( S.areArrows() && S.getSlidesNumber() > S.getSlidesAtOnce() ) {

		let arrowsContainer;

		// Create arrows container.
		if ( ! S.getArrowsContainer() ) {
			arrowsContainer = document.createElement( 'div' );
			arrowsContainer.classList.add( S.getClasses().arrowsContainer );
		}

		// Create arrows.
		for ( let i = 0; i < 2; i++ ) {

			const
			arrow        = document.createElement( 'button' ),
			icon         = document.createElement( 'i' ),
			iconClasses  = i === 0 ? [ 'fa', 'fa-arrow-left' ] : [ 'fa', 'fa-arrow-right' ];

			// Add icon.
			arrow.appendChild( icon );
			icon.classList.add( ...iconClasses );

			// Add events and attributes.
			arrow.setAttribute( 'type', 'button' );
			arrow.setAttribute( 'data-direction', i === 0 ? 'prev' : 'next' );
			arrow.addEventListener( 'click', changeSlideByArrow );
			arrow.classList.add( S.getClasses().arrows[ i ] );

			// Append arrow to the arrows container.
			if ( ! S.getArrowsContainer() ) {
				arrowsContainer.appendChild( arrow );
			} else {
				S.getArrowsContainer().appendChild( arrow );
			}

		}

		if ( ! S.getArrowsContainer() ) {
			// Append arrows to the DOM.
			S.getInterface().appendChild( arrowsContainer );
			// Add arrows container reference to variable.
			S.setArrowsContainer( arrowsContainer );
		}

	}

}

/**
 * Removes slider arrows.
 */
export function removeArrows() {

	const arrowsContainer = S.getArrowsContainer();

	if ( arrowsContainer ) {
		arrowsContainer.innerHTML = '';
	}

}

/**
 * Rebuild slider arrows.
 */
export function rebuildArrows() {

	removeArrows();
	createArrows();

}
