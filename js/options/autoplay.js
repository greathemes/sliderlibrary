import { Slider as S } from '../slider-class';
import { sliderPosition } from '../utilities/slider';
import { activateSlides } from '../utilities/slides';
import { deactivateDot, activateDot } from '../options/dots';
import { autoHeight } from '../options/auto-height';
import { rewind } from '../options/rewind';

/**
 * Clear autoplay interval.
 */
export function clearAutoplayInterval() {

	if ( S.isAutoplay() ) {

		S.getContainer().classList.remove( S.getClasses().container + '--autoplay' );
		clearInterval( S.getAutoplay() );
		S.setAutoplay( setInterval( autoplayInterval, S.getAutoplayTime() ) );

		setTimeout( function() {
			S.getContainer().classList.add( S.getClasses().container + '--autoplay' );
		}, S.getSpeed() );

	}

}

/**
 * Autoplay interval.
 */
export function setAutoplayInterval() {

	if ( S.isAutoplay() ) {

		S.setAutoplay( setInterval( autoplayInterval, S.getAutoplayTime() ) );

	}

}

/**
 * Autoplay mode.
 */
function autoplayInterval() {

	if ( S.isAutoplay() ) {

		const
		firstIndex = S.getFirstSlideIndex(),
		maxIndex   = S.getMaxSlideIndex(),
		isRewind   = S.isRewind();
		
		deactivateDot();

		if ( firstIndex === maxIndex && isRewind ) {
			rewind( 'next' );
		} else if ( firstIndex < maxIndex ) {
			S.incrementDot();
			S.incrementSlideIndexes();
		}

		activateDot();
		activateSlides();
		sliderPosition();
		autoHeight();

	}

}
