import { Slider as S } from '../slider-class';

/**
 * Rewind the slider.
 */
export function rewind( direction ) {

	const maxIndex = S.getMaxSlideIndex();

	if ( direction === 'prev' ) {
		S.incrementSlideIndexes( maxIndex );
		S.setDotIndex( maxIndex );
	} else {
		S.decrementIndexes( maxIndex );
		S.setDotIndex( 0 );
	}

}
