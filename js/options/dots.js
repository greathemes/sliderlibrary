import { Slider as S } from '../slider-class';
import { changeSlideByButton } from '../events/dots';
import { isPositiveOrZero } from '../utilities/number';

/**
 * Creates dots for slider.
 */
export function createDots() {

	if ( S.areDots() && S.getSlidesNumber() > S.getSlidesAtOnce() ) {

		let dotsContainer;

		// Create dots container.
		if ( ! S.getDotsContainer() ) {
			dotsContainer = document.createElement( 'div' );
			dotsContainer.classList.add( S.getClasses().dotsContainer );
		}

		// Create dots.
		const dotsNumber = S.getMaxSlideIndex() + 1;

		for ( let i = 0; i < dotsNumber; i++ ) {

			const	dot = document.createElement( 'button' );

			// Add events and attributes.
			dot.setAttribute( 'type', 'button' );
			dot.setAttribute( 'data-index', i );
			dot.classList.add( S.getClasses().dots );
			dot.addEventListener( 'click', changeSlideByButton );

			// Append dot to the dots container.
			if ( ! S.getDotsContainer() ) {
				dotsContainer.appendChild( dot );
			} else {
				S.getDotsContainer().appendChild( dot );
			}

		}

		// Append dots to the DOM.
		if ( ! S.getDotsContainer() ) {
			S.getInterface().appendChild( dotsContainer );
			S.setDotsContainer( dotsContainer );
		}

		// Add dot index and reference to dots container to Slider object.
		S.setDotIndex( S.getDotIndex() !== undefined ? S.getDotIndex() : S.getFirstSlideIndex() );

		while ( S.getDots( S.getDotIndex() ) === undefined ) {
			if ( S.getDots().length <= S.getDotIndex() ) {
				S.decrementDot();
			} else {
				S.incrementDot();
			}
		}

		// Activate dot.
		activateDot();

	}

}

/**
 * Remove slider dots.
 */
export function removeDots() {

	const dotsContainer = S.getDotsContainer();

	if ( dotsContainer ) {
		dotsContainer.innerHTML = '';
	}

}

/**
 * Rebuild dots for slider.
 */
export function rebuildDots() {

	removeDots();
	createDots();

}

/**
 * Deactivate dot.
 */
export function deactivateDot() {

	const
	dotIndex = S.getDotIndex(),
	dot      = isPositiveOrZero( dotIndex ) ? S.getDots( dotIndex ) : undefined;

	if ( dot ) {
		dot.classList.remove( S.getClasses().dotsActive );
	}

}

/**
 * Activate dot.
 */
export function activateDot() {

	const
	dotIndex = S.getDotIndex(),
	dot      = isPositiveOrZero( dotIndex ) ? S.getDots( dotIndex ) : undefined;

	if ( dot ) {
		dot.classList.add( S.getClasses().dotsActive );
	}

}
