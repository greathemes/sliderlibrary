import { Slider as S } from '../slider-class';
import { isPositiveOrZero } from '../utilities/number';

/**
 * Sets slide indexes.
 *
 * @param {Array} value
 */
export function setSlideIndexes() {

	S.setSlideIndexes = ( value ) => this.indexes = value;

}

/**
 * Sets cloned slide indexes.
 *
 * @param {Array} value
 */
export function setClonedSlideIndexes() {

	this.clonedSlideIndexes = [];

	S.setClonedSlideIndexes = ( value ) => this.clonedSlideIndexes = value;

}

/**
 * Sets cloned slides.
 *
 * @param {Array} value
 */
export function setClonedSlides() {

	S.setClonedSlides = ( value ) => this.cloned = value;

}

/**
 * Sets slide indexes depending on current slidesAtOnce parameter.
 */
export function setInitialSlideIndexes() {

	this.indexes = [ ...Array( S.getSlidesAtOnce() ).keys() ];

	S.setInitialSlideIndexes = () => this.indexes = [ ...Array( S.getSlidesAtOnce() ).keys() ];

}

/**
 * Sets reference to dots container.
 *
 * @param {Object} value - DOM element.
 */
export function setDotsContainer() {

	S.setDotsContainer = ( value ) => this.dotsContainer = value;

}

/**
 * Sets the maximum possible slide index.
 *
 * @param {Number} value - optional.
 */
export function setMaxIndex() {

	S.setMaxIndex = ( value ) => {
		this.maxIndex = value ? value : S.getSlidesNumber() - S.getSlidesAtOnce();
	}

}

/**
 * Sets index of the dot, which has to be active now.
 *
 * @param {Number} value.
 */
export function setDotIndex() {

	S.setDotIndex = ( value ) => this.dotIndex = value;

}

/**
 * Sets how many slides should be displayed simultaneously.
 *
 * @param {Number} value.
 */
export function setSlidesAtOnce() {

	S.setSlidesAtOnce = ( value ) => this.settings.slidesAtOnce = value;

}

/**
 * Sets slide indexes by incrementing.
 *
 * @param {Number} value - optional - We can increase the value by more than 1.
 */
export function incrementSlideIndexes() {

	S.incrementSlideIndexes = ( value = 1 ) => {
		this.indexes = this.indexes.map( n => n + value );
	}

}

/**
 * Sets slide indexes by decrementing.
 *
 * @param {Number} value - optional - We can decrease the value by more than 1.
 */
export function decrementIndexes() {

	S.decrementIndexes = ( value = 1 ) => {
		this.indexes = this.indexes.map( n => n - value );
	}

}

/**
 * Sets dot index by incrementing.
 */
export function incrementDot() {

	S.incrementDot = () => {
		this.dotIndex = this.dotIndex + 1;
	}

}

/**
 * Sets dot index by decrementing.
 */
export function decrementDot() {

	S.decrementDot = () => {
		this.dotIndex = this.dotIndex - 1;
	}

}

/**
 * Sets reference to arrows container.
 */
export function setArrowsContainer() {

	S.setArrowsContainer = ( value ) => this.arrowsContainer = value;

}

/**
 * Sets reference to main container.
 */
export function setContainer() {

	S.setContainer = ( value ) => this.container = value;

}

/**
 * Sets reference to interface container.
 */
export function setInterface() {

	S.setInterface = ( value ) => this.interface = value;

}

/**
 * Sets reference to autoplay interval.
 */
export function setAutoplay() {

	S.setAutoplay = ( value ) => this.settings.autoplay = value;

}
