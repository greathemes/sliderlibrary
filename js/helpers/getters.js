import { Slider as S } from '../slider-class';
import { isPositiveOrZero } from '../utilities/number';

/**
 * Returns the slider selector.
 */
export function getSelector() {

	S.getSelector = () => this.selector;

}

/**
 * Returns slide indexes.
 *
 * @param {Number} index - Index of specific slide index.
 */
export function getSlideIndexes() {

	S.getSlideIndexes = ( index ) => isPositiveOrZero( index ) ? this.indexes[ index ] : this.indexes;

}

/**
 * Returns the first slide index.
 */
export function getFirstSlideIndex() {

	S.getFirstSlideIndex = () => this.indexes[0];

}

/**
 * Returns the maximum slide index.
 */
export function getMaxSlideIndex() {

	S.getMaxSlideIndex = () => this.maxIndex;

}

/**
 * Returns slides.
 *
 * @param {Number} index - Index of specific slide.
 */
export function getSlides() {

	S.getSlides = ( index ) => isPositiveOrZero( index ) ? S.getSelector().children[ index ] : S.getSelector().children;

}

/**
 * Returns cloned slide indexes.
 */
export function getClonedSlideIndexes() {

	S.getClonedSlideIndexes = () => this.clonedSlideIndexes;

}

/**
 * Returns cloned slides.
 */
export function getClonedSlides() {

	this.cloned = [];

	S.getClonedSlides = () => this.cloned;

}

/**
 * Returns the number of slides displayed simultaneously.
 */
export function getSlidesAtOnce() {

	S.getSlidesAtOnce = () => this.settings.slidesAtOnce;

}

/**
 * Returns the number of slides.
 */
export function getSlidesNumber() {

	S.getSlidesNumber = () => S.getSelector().childElementCount;

}

/**
 * Returns the arrows container.
 */
export function getArrowsContainer() {

	S.getArrowsContainer = () => this.arrowsContainer;

}

/**
 * Returns the dots container.
 */
export function getDotsContainer() {

	S.getDotsContainer = () => this.dotsContainer;

}

/**
 * Returns dots or one dot.
 *
 * @param {Number} index - Index of specific dot.
 */
export function getDots() {

	S.getDots = ( index ) => isPositiveOrZero( index ) ? S.getDotsContainer().children[ index ] : S.getDotsContainer().children;

}

/**
 * Returns the current active dot index.
 */
export function getDotIndex() {

	S.getDotIndex = () => {
		const dotIndex = parseInt( this.dotIndex );
		return isPositiveOrZero( dotIndex ) ? dotIndex : undefined;
	}

}

/**
 * Returns the class names.
 */
export function getClasses() {

	S.getClasses = () => {
		return {
			dotsContainer  : this.settings.dotsContainerClass,
			dots           : this.settings.dotsClass,
			dotsActive     : this.settings.dotsActiveClass,
			container      : this.settings.containerClass,
			arrowsContainer: this.settings.arrowsContainerClass,
			arrows         : [ this.settings.arrowsPrevClass, this.settings.arrowsNextClass ],
			activeSlides   : this.settings.activeSlidesClass,
		}
	}

}

/**
 * Returns the breakpoints details.
 */
export function getBreakpoints() {

	S.getBreakpoints = () => this.settings.breakpoints;

}

/**
 * Returns the slider speed.
 */
export function getSpeed() {

	S.getSpeed = () => this.settings.speed;

}

/**
 * Returns the main container.
 */
export function getContainer() {

	S.getContainer = () => this.container;

}

/**
 * Returns the interface.
 */
export function getInterface() {

	S.getInterface = () => this.interface;

}

/**
 * Returns reference to autoplay interval.
 */
export function getAutoplay() {

	S.getAutoplay = () => this.settings.autoplay;

}

/**
 * Returns an autoplay time.
 */
export function getAutoplayTime() {

	S.getAutoplayTime = () => this.settings.autoplayTime;

}
