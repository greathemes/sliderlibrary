import { Slider as S } from '../slider-class';

/**
 * Returns true/false depending on whether the arrows exist.
 */
export function areArrows() {

	S.areArrows = () => this.settings.arrows;

}

/**
 * Returns true/false depending on whether the dots exist.
 */
export function areDots() {

	S.areDots = () => this.settings.dots;

}

/**
 * Returns true/false depending on whether the rewind is enabled.
 */
export function isRewind() {

	S.isRewind = () => this.settings.rewind;

}

/**
 * Returns true/false depending on whether a mobile first is enabled.
 */
export function isMobileFirst() {

	S.isMobileFirst = () => this.settings.mobileFirst;

}

/**
 * Returns true/false depending on whether an auto height is enabled.
 */
export function isAutoHeight() {

	S.isAutoHeight = () => this.settings.autoHeight;

}

/**
 * Returns true/false depending on whether an autoplay is enabled.
 */
export function isAutoplay() {

	S.isAutoplay = () => this.settings.autoplay;

}
