import yargs from 'yargs';
import webpack from 'webpack';

module.exports = {
	mode: yargs.argv.prod ? 'production' : 'development',
	entry: [
		'@babel/polyfill',
		'./js/slider.js'
	],
	output: { 
		filename: 'slider.js'
	},
	module: {
		rules: [
			{
				test: /\.js$/,
				exclude: /node_modules\/(?!bullets-js)/,
				use: {
					loader: 'babel-loader',
					options: {
						presets: [ '@babel/preset-env' ]
					}
				}
			}
		]
	}
}
