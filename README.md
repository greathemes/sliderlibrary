A small slider library written mainly for learning ES6. Should be compatible with IE11, Edge, Chrome, Firefox, Safari and Opera.

The slider has several options:

- Show/hide slider dots.
- Show/hide slider arrows.
- Breakpoints.
- Mobile first approach for breakpoints.
- Number of slides displayed simultaneously.
- Rewind.
- Auto height.
- Autoplay.

I'm working on adding swipe mode and animations.

The demo is here http://maciejruminski.com/slider

Used photos thanks to the authors:

https://unsplash.com/photos/phIFdC6lA4E
https://unsplash.com/photos/HRA_VAi9_Nc
https://unsplash.com/photos/Y8lCoTRgHPE
https://unsplash.com/photos/eluzJSfkNCk
https://unsplash.com/photos/xfngap_DToE
